import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { PeopleDbContext } from '../shared/DbContext';


const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
        const logger = (log: string) => context.log(log);
        let body = {};
        let status = 200;

        try {
            logger('Action - run');

            const connectionString = process.env['PeopleDb'];
            const peopleDb = new PeopleDbContext(logger, connectionString);
            const id = +req.params.id;
            const person =  await peopleDb.getPerson(id);

            logger('Action - done');

            body = {
                success: true,
                person
            };
        }
        catch (exception) {
            logger('Action - fail');

            body = {
                success: false,
                exception
            }
            status = 500;
        }

        context.res = {
            status,
            body
        }
};

export default httpTrigger;
