import { ConnectionPool, Request, IRecordSet, IResult } from 'mssql';
const parse = require('mssql-connection-string');

const TABLE_NAME = 'People2';
enum ContextLogs {
    PARSE = 'Parsing connection string',
    PARSED = 'Connection string parsed',
    CONNECT = 'Connecting to the database',
    CONNECTED = 'Connected with the database',
}

export class PeopleDbContext {

    private config: any;

    constructor(
        private logger: Function,
        connectionString: string
    ) {
        this.setConfig(connectionString);
    }

    public async getPeople(): Promise<IRecordSet<any>> {
        const result = await this.query(`select * from ${TABLE_NAME}`);

        return result.recordset;
    }

    public async getPerson(id: number): Promise<any> {
        const records = await this.query(`select * from ${TABLE_NAME} where PersonId=${id}`);
        const [ person ] = records.recordset;

        return person;
    }

    public async addPerson(person: any): Promise<any> {
        const { FirstName, LastName, PhoneNumber } = person;
        this.logger(`Saving person ${JSON.stringify(person)}`);

        await this.query(
            `insert into ${TABLE_NAME} (FirstName, LastName, PhoneNumber) values ('${FirstName}', '${LastName}', '${PhoneNumber}')`
        );
    }

    public async deletePerson(id: number): Promise<void> {
        await this.query(`delete from ${TABLE_NAME} where PersonId=${id}`);
    }

    private setConfig(connectionString: string): void {
        this.logger(ContextLogs.PARSE);
        this.config = parse(connectionString);
        this.logger(ContextLogs.PARSED);
    }

    private async getConnection() {
        this.logger(ContextLogs.CONNECT);
        const connection = await new ConnectionPool(this.config).connect();
        this.logger(ContextLogs.CONNECTED);

        return connection;
    }

    private async query(query: string): Promise<IResult<any>> {
        const connection = await this.getConnection();
        const request = new Request(connection);

        return await request.query(query);
    }

}
