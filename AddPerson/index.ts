import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { PeopleDbContext } from '../shared/DbContext';


const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
        const logger = (log: string) => context.log(log);
        let body = {};
        let status = 201;

        try {
            logger('Action - run');

            const connectionString = process.env['PeopleDb'];
            const peopleDb = new PeopleDbContext(logger, connectionString);
            const personToAdd = req.body;
            await peopleDb.addPerson(personToAdd);

            logger('Action - done');

            body = { success: true };
        }
        catch (exception) {
            logger('Action - fail');

            body = {
                success: false,
                exception
            }
            status = 500;
        }

        context.res = {
            status,
            body
        }
};

export default httpTrigger;
